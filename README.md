﻿# **Tiny URL**

**[tst20128-001-site1.atempurl.com/tinyurl/home](http://tst20128-001-site1.atempurl.com/TinyUrl/Home)**

Software used:

C#, EF(CodeFirst), MS SQL Server, Ajax, jQuery, Bootstrap

---------------------------------------------------------------
**Exercise:**

Design and implement a tinyurl (shortened URL) application.  The application should:

*  Accept as input a regular URL and return a tinyurl associated with the input URL.
* Accept as input a previously generated tinyurl and return the full URL associated with it

You can use any algorithm desired for generating the tinyurl and it need not be universally unique. Please do not use an existing public API.

Constraints:

* Application must be coded in C#, MVC
* Application should be complete front to back end solution including DB.
* Application needs to be browser based
* Solution file and source code should be self-contained such that we can compile and run the application here.

---------------------------------------------------------------

# **Screenshots**

A solution is to use the integer id stored in database and convert the integer to character string. A URL alias  character can be  an  upper/lover case alphabet, a digit. There are 62 possible characters: 26 + 26 + 10 = 62   

The task is to convert a decimal number to base 62 number and vice versa.   
1          -> a   
27         -> A   
1640 -> AB


## **Home**



![TinyUrl1.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyUrl1.jpg)


---------------------------------------------------------------

## **Wrong URL**

![TinyURL043.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyURL04.jpg)

---------------------------------------------------------------

## **Wrong alias**

![TinyURL03.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyURL03.jpg)


---------------------------------------------------------------

## **Admin**
![TinyUrl4.png](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyUrl4.png)

---------------------------------------------------------------

## **Redirect**

![TinyURL11.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyURL11.jpg)

![TinyURL12.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyURL12.jpg)

---------------------------------------------------------------

## **Unable to redirect**

![TinyURL10.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyURL10.jpg)

---------------------------------------------------------------

## **Wrong alias**


![TinyURL9.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/TinyURL9.jpg)

---------------------------------------------------------------