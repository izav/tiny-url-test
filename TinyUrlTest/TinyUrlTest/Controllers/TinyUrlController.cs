﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using TinyUrlTest.Models.TinyUrl;
using TinyUrlTest.Repositories.TinyUrl;
using TinyUrlTest.ViewModels;
using TinyUrlTest.Services;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TinyUrlTest.Controllers
{
    public class TinyUrlController : Controller
    {
        public const int ConstItemsPerPage = 10;        
        private ITinyUrlRepository<UrlMap, int> _repo;


        public TinyUrlController(ITinyUrlRepository<UrlMap, int> repo)
       {
           _repo = repo;
       }


        public ActionResult RedirectToFullUrl(string id)
        {
            var tinyUrl = id;
            if (tinyUrl.Count() > UrlMap.MaxDigitNumber)
                return View("RedirectError",
                            (object)("Wrong alias. Alias max length can't be more than " +  UrlMap.MaxDigitNumber));
               

            var cacheService = new CacheService();
            var urlMap = cacheService.GetOrSet("TinyUrl-"+ tinyUrl, 
                                () =>_repo.GetUrlMapById(UrlMap.GetIdByTinyUrl(tinyUrl)));
            if (urlMap != null)
            {
                return Redirect(urlMap.LongUrl);
            }
            else
            {
                return View("RedirectError",
                            (object)("No Long Url for alias '" + tinyUrl + "' was not found. Not able to redirect."));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteUrlMap(int id)
        {
            var cacheService = new CacheService();
            string tinyUrl = UrlMap.GetTinyUrlById(id);
            var urlMap = cacheService.Remove<UrlMap>("TinyUrl-" + tinyUrl);
            var result = _repo.DeleteUrlMap(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Home()
        {
            ViewBag.Host = Request.Url.Scheme + "://" + Request.Url.Authority + "/";
            return View(new UrlMap());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MakeTinyUrl(UrlMap urlMap)
        {           
           urlMap.LongUrl = UrlMap.MakeUrlWithProtocol(urlMap.LongUrl);
           var result = IsUrlExists(urlMap.LongUrl);
       
            if (result)
            {
                urlMap = _repo.AddUrlMap(urlMap);
            }
            else {
                urlMap.Id = -3;
            }
            return Json( urlMap, JsonRequestBehavior.AllowGet);
       }

      
        [NonAction]
        public bool IsUrlExists(string FullUrl)
        {
            string url = UrlMap.MakeUrlWithProtocol(FullUrl);
            bool result;
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                result = (response.StatusCode == HttpStatusCode.OK);
               
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public ActionResult Index(int id = 1, int itemsPerPage = ConstItemsPerPage)
        {         
            var pageNumber = id;
            var urlMaps = _repo.GetUrlMaps(pageNumber, itemsPerPage);
            var urlMapsCount = _repo.GetUrlMapsCount();

            var vm = new UrlMapsViewModel(){
                    Host = Request.Url.Scheme + "://" + Request.Url.Authority + "/",
                    PageNumber = pageNumber,
                    PagesNumber = (int)Math.Ceiling((double)urlMapsCount / itemsPerPage),
                    ItemsPerPage = itemsPerPage,
                    UrlMaps = urlMaps,
                    UrlMapsCount = urlMapsCount
                };

            if (Request.IsAjaxRequest())
            {
                return Json(new {gridInfo = vm }, JsonRequestBehavior.AllowGet);
            }
           
            else return View(vm);
        }
    }
}