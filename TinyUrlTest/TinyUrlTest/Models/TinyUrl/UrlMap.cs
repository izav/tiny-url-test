﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq.Expressions;

namespace TinyUrlTest.Models.TinyUrl
{ 
    public class UrlMap
    {
        public const int MaxDigitNumber = 5;       
        public static readonly int MaxId = GetMaxId();      
        public int Id { get; set; }
             
        [Required]
        [DisplayName("Long  Url")]
        public string LongUrl { get; set; }

        [StringLength(MaxDigitNumber)]
        [DisplayName("Short Url")]
        [RegularExpression("^[0-9a-zA-Z ]+$", ErrorMessage = "Use characters and numbers only")]
        public string TinyUrl { get; set; }
       
        public const string MapCollection = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        
        public static string GetTinyUrlById(int n) {
            n = n - 1;
            List<char> tinyUrl = new List<char>();
            if (n == 0) return "a";
            while (n >   0)
            {
                tinyUrl.Add(MapCollection[n % 62]);
                n = n / 62;
            }
            tinyUrl.Reverse();

            return string.Join("", tinyUrl.ToArray());
        }

        public static int GetIdByTinyUrl(string tinyUrl)
        {       
            int id = 0; 
            for (int i = 0; i < tinyUrl.Count(); i++)
            {
                if ('a' <= tinyUrl[i] && tinyUrl[i] <= 'z')
                    id = id * 62  +tinyUrl[i] - 'a';
                if ('A' <= tinyUrl[i] && tinyUrl[i] <= 'Z')
                    id = id * 62  +tinyUrl[i] - 'A' + 26;
                if ('0' <= tinyUrl[i] && tinyUrl[i] <= '9')
                    id = id * 62  + tinyUrl[i] - '0' + 52;
            }
            return id +1 ;
        }

        public static int GetMaxId()
        {
            var maxId = Int32.MaxValue;
            var maxTinyUrlForInt32 = GetTinyUrlById(maxId);

            //Check if const MaxDigitNumber is set right for Int32
            var isMaxDigitNumberTooBigForInt32 = maxTinyUrlForInt32.Count() < MaxDigitNumber ||
                String.Compare(maxTinyUrlForInt32, new String('9', MaxDigitNumber)) < 0;
           
            if (isMaxDigitNumberTooBigForInt32)
            {
                var maxDigitNumberForInt32 = maxTinyUrlForInt32.Count() - 
                    (maxTinyUrlForInt32 == new String('9', maxTinyUrlForInt32.Count()) ? 0 : 1);

                throw new System.ArgumentException("const int UrlMap.MaxDigitNumber = " +
                    MaxDigitNumber + " can't be more than " + maxDigitNumberForInt32);                
            }
           
            maxId = GetIdByTinyUrl(new String('9', MaxDigitNumber));                
           
            return maxId;
          
        }

        public static string MakeUrlWithProtocol(string url){
            url = url.ToLower();
            if (!(url.Length > 5 && (url.StartsWith("http://") || url.StartsWith("https://"))))
            {
                url = "http://" + url;
            }
            return url;
        }
    }
}