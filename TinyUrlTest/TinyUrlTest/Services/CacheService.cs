﻿using System;
using System.Web;
using System.Runtime.Caching;  

namespace TinyUrlTest.Services
{
    public class CacheService : ICacheService
    {
        public T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class
        {
            T item = MemoryCache.Default.Get(cacheKey) as T;
            if (item == null && getItemCallback != null)
            {
                item = getItemCallback();
                if (item != null) {
                    MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(10));
                }
            }
            return item;
        }

        public T Remove<T> (string cacheKey) where T : class 
        {
            T item = MemoryCache.Default.Remove(cacheKey) as T;
           return  item;
        }
    }
    
    interface ICacheService
    {
        T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class;
        T Remove<T>(string cacheKey) where T : class;
    }
}