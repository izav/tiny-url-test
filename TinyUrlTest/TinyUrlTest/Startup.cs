﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using TinyUrlTest.Repositories;
using TinyUrlTest.Controllers;

[assembly: OwinStartupAttribute(typeof(TinyUrlTest.Startup))]
namespace TinyUrlTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            using (var context = new DbContextEF())
            {
                context.Database.Initialize(false);
            }
        }
    }
}
