﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using TinyUrlTest.Models.TinyUrl;
using TinyUrlTest.Models;


namespace TinyUrlTest.Repositories
{ 
    public class DbContextEFInitializer : DropCreateDatabaseIfModelChanges<DbContextEF>
    {
        protected override void Seed(DbContextEF context)
        {
            InitializeDB(context);           
            base.Seed(context);
        }
     
        private void InitializeDB(DbContextEF context)
        {
            string[] links = new string[] { 
                "https://bitbucket.org/irinazav/project-test-signal-r/src",
                "https://bitbucket.org/irinazav/test-pyramid/src",
                "https://bitbucket.org/irinazav/test-olo/src",
                "https://bitbucket.org/irinazav/linqpad-test/src"};

            //by Id -> 1, 2 3,4
            var tinyUrls_1 = links.Select((link, index) => new UrlMap()
            {
                LongUrl = link,
                TinyUrl = UrlMap.GetTinyUrlById(index + 1),
                Id = index + 1,
            }).ToList();

            //by TinyURL -> Abc10, Abc11, Abc12, Abc13
            var tinyUrls_2 = links.Select((link, index) => new UrlMap()
            {
                LongUrl = link,
                TinyUrl = "Abc1" + index,
                Id = UrlMap.GetIdByTinyUrl("Abc1" + index),
            }).ToList();

            var nextId = tinyUrls_2.LastOrDefault().Id + 1;

            //continue from nextId. TinyURLs must be Abc14 , Abc15 , Abc16, Abc17
            var tinyUrls_3 = links.Select((link, index) => new UrlMap()
            {
                LongUrl = link,
                TinyUrl = UrlMap.GetTinyUrlById(index + nextId),
                Id = index + nextId,
            }).ToList();

            context.UrlMaps.AddRange(tinyUrls_1);
            context.UrlMaps.AddRange(tinyUrls_2);
            context.UrlMaps.AddRange(tinyUrls_3);

            context.SaveChanges();
        
        }
    }
}