﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TinyUrlTest.Models.User;
using TinyUrlTest.Models.TinyUrl;
using TinyUrlTest.Models;


namespace TinyUrlTest.Repositories
{
    public class DbContextEF : IdentityDbContext<ApplicationUser>
    {
        public DbSet<UrlMap> UrlMaps { get; set; }

        public DbContextEF(): base("DefaultConnection")
        { 
             this.Configuration.LazyLoadingEnabled = true; 
         }

        public static DbContextEF Create()
        {
            return new DbContextEF();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UrlMap>()
            .Property(m => m.Id)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

        }
    }
}