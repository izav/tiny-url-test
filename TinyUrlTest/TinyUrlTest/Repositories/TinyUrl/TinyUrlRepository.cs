﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TinyUrlTest.Models.TinyUrl;


namespace TinyUrlTest.Repositories.TinyUrl
{
    public class TinyUrlRepository : ITinyUrlRepository<UrlMap, int>
    {
        private DbContextEF _context;

        public TinyUrlRepository(DbContextEF context)
        {
            _context = context;
        }

        public IEnumerable<UrlMap> GetUrlMaps(int pageNumber, int itemsPerPage ) {  
         
            return _context.UrlMaps.OrderBy(c => c.Id)
            .        Skip(itemsPerPage * (pageNumber - 1))
                    .Take(itemsPerPage)                  
                    .ToList();
        }

        public int GetUrlMapsCount()
        {
            return _context.UrlMaps.Count();
        }

        public UrlMap GetUrlMapById(int id)
        {
            return _context.UrlMaps.Find(id);
        }
 

        public UrlMap GetUrlMapByTinyUrl(string tinyUrl)
        {
            int id = UrlMap.GetIdByTinyUrl(tinyUrl);
            return _context.UrlMaps.Find(id);           
        }

        public int GetNextId()
        {
            var urlMapsCount = _context.UrlMaps.Count();
            if (urlMapsCount == 0) return 1;
            else if (urlMapsCount == Int32.MaxValue) return 0;

            var url = _context.UrlMaps.OrderBy(link => link.Id)
                                        .AsEnumerable()
                                        .Select((link, index) => new {Index = index, Record = link})
                                        .SkipWhile(link => link.Record.Id == link.Index + 1)
                                        .FirstOrDefault();
            int newId;
            if (url == null)
            {
                var count = _context.UrlMaps.Count();
                newId = (count == UrlMap.MaxId) ? count + 1 : -2;
            }
            else newId = url.Record.Id -1;

          return newId;
        }

        public UrlMap AddUrlMap(UrlMap urlMap)
        {
            var temp = urlMap;
           
            if  (string.IsNullOrEmpty(urlMap.TinyUrl))
            {
                urlMap.Id = GetNextId();
                urlMap.TinyUrl = UrlMap.GetTinyUrlById(urlMap.Id);
            }
            else {
                urlMap.Id = UrlMap.GetIdByTinyUrl(urlMap.TinyUrl);           
            }
            //id = -2 -> ids are full
            if (urlMap.Id == -2) return urlMap;
            try { 
            _context.UrlMaps.Add(urlMap);            
            _context.SaveChanges();
            }
            catch
            {   // custom tinyurl exists - 0 / not able to save = -1 / full = -2
                urlMap.Id = (temp.TinyUrl == null || temp.TinyUrl == "") ? -1 : 0;
            } 
            return urlMap;
        }

        public int DeleteUrlMap(int id)
        {
           var  originalUrlMap =  _context.UrlMaps.Find(id);
           if (originalUrlMap != null){
               _context.UrlMaps.Remove(originalUrlMap);
               _context.SaveChanges();
               return id;
           }
           else  return 0;
        }
    }
}