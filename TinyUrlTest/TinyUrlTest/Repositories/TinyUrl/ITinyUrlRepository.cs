﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TinyUrlTest.Models.TinyUrl;



namespace TinyUrlTest.Repositories.TinyUrl
{
    public interface  ITinyUrlRepository <T, TId> where T : class      
    {
        IEnumerable<UrlMap> GetUrlMaps(int pageNo, int pageSize);
        int GetUrlMapsCount();
        UrlMap GetUrlMapById(int id);
        UrlMap GetUrlMapByTinyUrl(string tinyUrl);
        int GetNextId();
        UrlMap AddUrlMap(UrlMap urlMap);
        int DeleteUrlMap(int id);      
    }
}