﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TinyUrlTest.Models.TinyUrl;

namespace TinyUrlTest.ViewModels
{
    public class UrlMapsViewModel
    {
        public IEnumerable<UrlMap> UrlMaps { get; set; }
        public int UrlMapsCount { get; set; }
        public int ItemsPerPage { get; set; }
        public int PageNumber { get; set; }
        public int PagesNumber { get; set; }
        public string Host { get; set; }
    }
}